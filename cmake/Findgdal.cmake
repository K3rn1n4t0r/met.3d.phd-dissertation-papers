####################################################################################################
#  This file is a find module script for CMake to locate all required headers and
#  static libraries and to correctly setup Met.3D on every system
#
#  Package: GDAL
#       - GDAL_FOUND        - System has found the package
#       - GDAL_INCLUDE_DIR  - Package include directory
#       - GDAL_LIBRARIES    - Package static libraries
#
#  Copyright 2018 Marc Rautenhaus
#  Copyright 2018 Michael Kern
#  Copyright 2018 Bianca Tost
#
####################################################################################################

# Include common settings such as common include/library directories
include("cmake/common_settings.cmake")

# Set the name of the package
set(PKG_NAME gdal)

if (UNIX)
    use_pkg_config(${PKG_NAME})
endif (UNIX)

find_path(${PKG_NAME}_INCLUDE_DIR
        NAMES # Name of all header files
            gdal.h
        HINTS # Hints to the directory where the package is currently installed in
            $ENV{${PKG_NAME}_DIR}
            ${PKG_INCLUDE_DIRS}
        PATH_SUFFIXES # Subfolders in the install directory
            include
        PATHS # The directories where cmake should look for the files by default if HINTS does not work
            ${COMMON_INSTALL_DIRS}
        )

find_library(${PKG_NAME}_LIBRARY_RELEASE
        NAMES
            gdal GDAL
        HINTS
            $ENV{${PKG_NAME}_DIR}
            ${PKG_LIBRARY_DIRS}
        PATH_SUFFIXES
            lib64
            lib
        PATHS
            ${COMMON_INSTALL_DIRS}
        )

find_library(${PKG_NAME}_LIBRARY_DEBUG
        NAMES
            gdald GDALD
        HINTS
            $ENV{${PKG_NAME}_DIR}
            ${PKG_LIBRARY_DIRS}
        PATH_SUFFIXES
            lib64
            lib
        PATHS
            ${COMMON_INSTALL_DIRS}
        )

if (NOT ${PKG_NAME}_LIBRARY_DEBUG)
    message("Debug library for ${PKG_NAME} was not found")
    set(${PKG_NAME}_LIBRARY_DEBUG "${${PKG_NAME}_LIBRARY_RELEASE}")
endif()

if (NOT ${PKG_NAME}_LIBRARY_RELEASE)
    message("Release library for ${PKG_NAME} was not found")
    set(${PKG_NAME}_LIBRARY_RELEASE "${${PKG_NAME}_LIBRARY_DEBUG}")
endif()
        
if (${PKG_NAME}_LIBRARY_DEBUG AND ${PKG_NAME}_LIBRARY_RELEASE)
    # use different libraries for different configurations
    set (${PKG_NAME}_LIBRARIES
            optimized ${${PKG_NAME}_LIBRARY_RELEASE}
            debug ${${PKG_NAME}_LIBRARY_DEBUG})
elseif (${PKG_NAME}_LIBRARY_RELEASE)
    # if only release has been found, use that
    set (${PKG_NAME}_LIBRARIES
            ${${PKG_NAME}_LIBRARY_RELEASE})
endif ()

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set ${PGK_NAME}_FOUND to TRUE if
# all listed variables are TRUE
find_package_handle_standard_args(${PKG_NAME} REQUIRED_VARS ${PKG_NAME}_LIBRARIES ${PKG_NAME}_INCLUDE_DIR)
# Marks cmake cached variables as advanced
mark_as_advanced(${PKG_NAME}_INCLUDE_DIR ${PKG_NAME}_LIBRARIES)


# find_package(${PKG_NAME} REQUIRED)
# include_directories(${GDAL_INCLUDE_DIR})
# Check for gdal version >= 2.0
file(STRINGS "${${PKG_NAME}_INCLUDE_DIR}/gdal_version.h" ${PKG_NAME}_VERSION_FILE NEWLINE_CONSUME)
# Extract version from content by matching string: '"major.minor.patch"'.
string(REGEX MATCH "\"[0-9]+([.][0-9]+)?([.][0-9]+)?\"" ${PKG_NAME}_VERSION ${${PKG_NAME}_VERSION_FILE})
string(REPLACE "\"" "" ${PKG_NAME}_VERSION ${${PKG_NAME}_VERSION})
if(${${PKG_NAME}_VERSION} VERSION_LESS "2.0")
    message(FATAL_ERROR "GDAL library version " ${${PKG_NAME}_VERSION} " is not supported by Met.3D. Please update GDAL library to version 2.0 or later.")
endif()